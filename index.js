const nameInput = document.getElementById('name');
const stateNameInput = document.getElementById('stateName');
let addItems = document.getElementsByClassName('addItem');
const addItemsContainer = document.getElementById('addItemsContainer');
const btnAddItem = document.getElementById('btnAddItem');
const btnAddItemStart = document.getElementById('btnAddItemStart');
const btnAddItemEnd = document.getElementById('btnAddItemEnd');
const outReducer = document.getElementById('reducer');
const outSelector = document.getElementById('selector');
const outAction = document.getElementById('action');
const outActiontype = document.getElementById('actionType');
const btnCompile = document.getElementById('btnCompile');
let addItemEnd = true;

btnAddItemStart.addEventListener('click', () => {
    addItemEnd = false;
    console.log(addItemEnd);
});

btnAddItemEnd.addEventListener('click', () => {
    addItemEnd = true;
    console.log(addItemEnd);
});

btnCompile.addEventListener('click', () => {
    const reducerTemplate = `
        case types.${nameInput.value.toUpperCase()}:
            return {
                ...state,
                ${stateNameInput.value}: payload,
            };
        `;

    const actionTemplate = `
        export const set${stateNameInput.value[0].toUpperCase() +
            stateNameInput.value.slice(
                1
            )} = payload => ({ type: types.${nameInput.value.toUpperCase()}, payload });
    `;

    const selectorTemplate = `
        export const get${stateNameInput.value[0].toUpperCase() +
            stateNameInput.value.slice(1)} = state => state.${
        stateNameInput.value
    };
    `;

    const actionTypeTemplate = `
        export const ${nameInput.value.toUpperCase()} = ${'`'}${nameInput.value.toUpperCase()}${'`'}
    `;

    const addItemReducer = value => {
        const newType = addItemEnd
            ? nameInput.value.toUpperCase() + '_' + value.toUpperCase()
            : value.toUpperCase() + '_' + nameInput.value.toUpperCase();
        const newStateName =
            stateNameInput.value + value[0].toUpperCase() + value.slice(1);
        const addItemReducerTemplate = `
        case types.${newType}:
            return {
                ...state,
                ${newStateName}: payload,
            };
        `;
        return addItemReducerTemplate;
    };

    const addItemAction = value => {
        const newStateName =
            stateNameInput.value[0].toUpperCase() +
            stateNameInput.value.slice(1) +
            value[0].toUpperCase() +
            value.slice(1);

        const newType = addItemEnd
            ? nameInput.value.toUpperCase() + '_' + value.toUpperCase()
            : value.toUpperCase() + '_' + nameInput.value.toUpperCase();
        const addItemActionTemplate = `
        export const set${newStateName} = payload => ({ type: types.${newType}, payload });
        `;
        return addItemActionTemplate;
    };

    const addItemSelector = value => {
        const newStateName =
            stateNameInput.value[0].toUpperCase() +
            stateNameInput.value.slice(1) +
            value[0].toUpperCase() +
            value.slice(1);

        const addItemSelectorTemplate = `
        export const get${newStateName} = state => state.${newStateName[0].toLowerCase() +
            newStateName.slice(1)};
        `;
        return addItemSelectorTemplate;
    };

    const addItemActionType = value => {
        const newType = addItemEnd
            ? nameInput.value.toUpperCase() + '_' + value.toUpperCase()
            : value.toUpperCase() + '_' + nameInput.value.toUpperCase();

        const addItemAcionTypeTemplate = `
        export const ${newType} = ${'`'}${newType}${'`'}
        `;
        return addItemAcionTypeTemplate;
    };

    outReducer.value = reducerTemplate;
    outAction.value = actionTemplate;
    outSelector.value = selectorTemplate;
    outActiontype.value = actionTypeTemplate;
    for (let i = 0; i < addItems.length; i++) {
        if (!addItems[i].value) continue;
        outReducer.value += addItemReducer(addItems[i].value);
        outAction.value += addItemAction(addItems[i].value);
        outSelector.value += addItemSelector(addItems[i].value);
        outActiontype.value += addItemActionType(addItems[i].value);
    }
});

btnAddItem.addEventListener('click', () => {
    addItemsContainer.insertAdjacentHTML(
        'beforeend',
        `<input type="text" class="addItem" placeholder="Additional item" />`
    );
});
